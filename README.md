# CubeMX-Makefile-template  make flash

``` cpp
add section #Make flash  to file ~/STM32CubeMX/db/plugins/projectmanager/templates/Makefile.tpl

apt install libudev-dev libusb-1* gcc-arm-none-eabi gdb-arm-none-eabi

to allow access user 'urername' to usb to devices:

useradd -g dialout 'username'
addgroup --system 'username' dialout
addgroup --system 'username' root
addgroup --system 'username' plugdev
groups 'username'

STM32CubeMX:
nones:
for me, openjdk-11-jre, display of fonts is incorrect.

apt purge openjdk-*
apt install openjdk-8-jdk
apt install openjdk-8-jre
$ update-java-alternatives --list
# update-alternatives --config java

http://www.st.com/content/ccc/resource/technical/software/sw_development_suite/00/22/e1/cf/b7/83/42/25/stm32cubemx.zip/files/stm32cubemx.zip/jcr:content/translations/en.stm32cubemx.zip


STM32CubeProgrammer:
apt install openjfx
https://www.st.com/content/ccc/resource/technical/software/utility/group0/3c/80/a7/a1/72/51/4f/38/stm32cubeprog/files/stm32cubeprog.zip/_jcr_content/translations/en.stm32cubeprog.zip

or this link
https://www.st.com/content/ccc/resource/technical/software/utility/group0/3c/80/a7/a1/72/51/4f/38/stm32cubeprog/files/stm32cubeprog.zip/jcr:content/translations/en.stm32cubeprog.zip

# new path release:

nano $HOME/.stm32cubemx/databases/DB.4.0.220/db/plugins/projectmanager/templates/Makefile.tpl
nano $HOME/.stm32cubemx/databases/DB.4.0.221/db/plugins/projectmanager/templates/Makefile.tpl

#OPT = -OPT_LEVEL
#OPT = -O0
#OPT = -O1
#OPT = -O2
#OPT = -O3
#OPT = -g3
#OPT = -ggdb

possible add /usr/bin to binpath:
BINPATH = /usr/bin

#######################################
# Make flash
#	"write through st-link v2"
# Make stm32flash:
#	"write through uart /dev/ttyUSB0"
#######################################
flash:
	st-flash --reset erase
	st-flash --reset write $(BUILD_DIR)/$(TARGET).bin 0x8000000

stm32flash:
	stm32flash -w $(BUILD_DIR)/$(TARGET).bin -v -g 0x0 /dev/ttyUSB0

#######################################

```
